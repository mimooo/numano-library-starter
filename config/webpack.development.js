const webpack = require('webpack'),
    path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    webpackMerge = require('webpack-merge'),
    commonConfig = require('./webpack.common');

module.exports = function (env) {
    return webpackMerge(commonConfig(env), {
        entry: ['./styles.css', './index.html'],
        devtool: 'eval',
        devServer: {
            stats: 'minimal',
            hot: true,
            inline: true,
            historyApiFallback: true,
            contentBase: path.resolve(__dirname, '../src'),
            port: 3001,
            overlay: {
                errors: true,
                warnings: true
            }
        },
        module: {
            rules: [{
                    test: /\.html$/,
                    use: ['html-loader'],
                    include: [path.resolve(__dirname, '../src')]
                },
                {
                    test: /\.css$/,
                    use: ['style-loader',
                        'css-loader?importLoaders=1',
                        'postcss-loader'
                    ],
                    include: [path.resolve(__dirname, '../src')]
                }
            ]
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new HtmlWebpackPlugin({
                template: './index.html',
                inject: 'body'
            })
        ]
    })
}