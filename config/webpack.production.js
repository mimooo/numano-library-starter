const webpack = require('webpack'),
    path = require('path'),
    webpackMerge = require('webpack-merge'),
    commonConfig = require('./webpack.common'),
    CompressionPlugin = require('compression-webpack-plugin'),
    OptimizeJsPlugin = require('optimize-js-plugin');

module.exports = function (env) {
    return webpackMerge(commonConfig(env), {
        devtool: 'source-map',
        plugins: [
            new webpack.NoEmitOnErrorsPlugin(),
            // new webpack.optimize.UglifyJsPlugin({
            //     beautify: false,
            //     mangle: {
            //         screw_ie8: true,
            //         keep_fnames: true
            //     },
            //     compress: {
            //         warnings: false,
            //         screw_ie8: true
            //     },
            //     sourceMap: true,
            //     comments: false
            // }),
            new OptimizeJsPlugin({
                sourceMap: true
            }),
            new CompressionPlugin({
                asset: '[path].gz[query]',
                algorithm: 'gzip',
                test: /\.(js|html)$/,
                threshold: 10240,
                minRatio: 0.8
            })
        ]
    })
}